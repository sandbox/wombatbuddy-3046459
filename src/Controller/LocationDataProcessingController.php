<?php

namespace Drupal\user_location\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class LocationDataProcessingController.
 */
class LocationDataProcessingController extends ControllerBase {
  /**
   * @var PrivateTempStoreFactory $tempStore
   */
  protected $tempStore;

  /**
   * Class constructor.
   */
  public function __construct(PrivateTempStoreFactory $tempStore) {
    $this->tempStore = $tempStore;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private')
    );
  }

  /**
   * A location data processing.
   *
   * @param  Symfony\Component\HttpFoundation\Request $locationData
   * @return Symfony\Component\HttpFoundation\Response
   */
  public function processing(Request $request) {
    $locationData = json_decode($request->getContent());
    // Store location in session.
    $this->tempStore->get('user_location')->set('location_data', $locationData);
    // return new Response();
    // For testing purpose, returning a location data which was received from JavaScript.
    return new JsonResponse($locationData);
  }

   /**
   * Fetch a location data.
   *
   * @param  Symfony\Component\HttpFoundation\Request $locationData
   * @return Symfony\Component\HttpFoundation\Response
   */
  public function fetch(Request $request) {
    $locationData = $this->tempStore->get('user_location')->get('location_data');

    return new JsonResponse($locationData);
  }

}
